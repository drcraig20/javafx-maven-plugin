package com.drcraig.javafx.maven.plugin;

/**
 * Intellectual Property of and
 * Created by Tofade O. Gbenga for Lawpavilion Business Solutions
 * User: Dr Craig
 * Time/Date: 11:42 pm - 24 May 2020
 */
public class Resource {
    private String source;
    private String target;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "Resource{" +
            "source='" + source + '\'' +
            ", target='" + target + '\'' +
            '}';
    }
}
